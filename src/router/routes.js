
const routes = [
  {
    path: '/',
    component: () => import('layouts/main.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue') },
      { path: '/services', component: () => import('pages/services.vue') },
      { path: '/payments', component: () => import('pages/paymentMethods.vue') },
      { path: '/appoinment', component: () => import('pages/appoinment.vue') }
    ]
  },
  {
    path: '/auth',
    component: () => import('layouts/main.vue'),
    children: [
      { path: 'login', component: () => import('pages/auth/login.vue') }
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/admin/main.vue'),
    children: [
      {
        path: 'main',
        component: () => import('pages/admin/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-real',
        component: () => import('pages/admin/caseReal/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-real/add',
        component: () => import('pages/admin/caseReal/add.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-success',
        component: () => import('pages/admin/caseSuccess/index.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'case-success/add',
        component: () => import('pages/admin/caseSuccess/add.vue'),
        meta: {
          requiresAuth: true
        }
      },
      {
        path: 'newsletter',
        component: () => import('pages/admin/newsletter/index.vue'),
        meta: {
          requiresAuth: true
        }
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/error404.vue')
  }
]

export default routes
