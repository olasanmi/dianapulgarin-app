import { add, list, deleteCase, updateCase, getActives } from 'src/api/caseReal'

export function addCaseReal ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return add(data, (response) => {
      commit('setCaseReal', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function updateCaseReal ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return updateCase(data, (response) => {
      commit('setFilterCaseReal', {})
      commit('setCaseReal', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function getCases ({ commit }) {
  return new Promise((resolve, reject) => {
    return list((response) => {
      commit('setCaseReal', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function deleteCaseReal ({ commit }, id) {
  return new Promise((resolve, reject) => {
    return deleteCase(id, (response) => {
      commit('setCaseReal', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function filterCaseReal ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setFilterCaseReal', data)
    resolve(data)
  })
}

export function getCasesActives ({ commit }) {
  return new Promise((resolve, reject) => {
    return getActives((response) => {
      commit('setCaseReal', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
