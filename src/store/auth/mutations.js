export function setUser (user, data) {
  user.user = data.user
  user.token = data.token
}

export function logout (user) {
  user.user = {}
  user.token = null
}

export function setLocale (user, data) {
  user.locale = data
}
