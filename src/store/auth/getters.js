export function user (user) {
  return user.user
}

export function token (user) {
  return user.token
}

export function locale (user) {
  return user.locale
}
