import { login, logout, getClientData } from 'src/api/auth'

export function loginUser ({ commit }, data) {
  localStorage.removeItem('user')
  localStorage.removeItem('token')
  return new Promise((resolve, reject) => {
    return login(data, (response) => {
      localStorage.setItem('user', JSON.stringify(response.data.user))
      localStorage.setItem('token', JSON.stringify(response.data.token))
      commit('setUser', response.data)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function logoutUser ({ commit }, data) {
  return new Promise((resolve, reject) => {
    localStorage.removeItem('user')
    localStorage.removeItem('token')
    return logout(data, (response) => {
      commit('logout')
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function setLocale ({ commit }) {
  return new Promise((resolve, reject) => {
    return getClientData((response) => {
      if (response.data.ip.country_code === 'CO') {
        commit('setLocale', 'es')
      } else {
        commit('setLocale', 'en')
      }
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
