import { add, list, deleteCase, updateCase, getActives } from 'src/api/caseSuccess'

export function addCaseSuccess ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return add(data, (response) => {
      commit('setCaseSuccess', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function updateCaseSuccess ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return updateCase(data, (response) => {
      commit('setFilterCaseSuccess', {})
      commit('setCaseSuccess', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function getCases ({ commit }) {
  return new Promise((resolve, reject) => {
    return list((response) => {
      commit('setCaseSuccess', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function deleteCaseSuccess ({ commit }, id) {
  return new Promise((resolve, reject) => {
    return deleteCase(id, (response) => {
      commit('setCaseSuccess', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function filterCaseSuccess ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setFilterCaseSuccess', data)
    resolve(data)
  })
}

export function getCasesActives ({ commit }) {
  return new Promise((resolve, reject) => {
    return getActives((response) => {
      commit('setCaseSuccess', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
