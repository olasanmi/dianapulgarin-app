import axios from 'axios'

import { domain } from './data'

const url = domain

export function add (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/CaseReal', data: data, headers: { 'Accept': 'application/json; charset=utf-8', 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function updateCase (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/CaseReal/' + data.id, data: data.form, headers: { 'Accept': 'application/json; charset=utf-8', 'Content-Type': 'multipart/form-data', 'Authorization': `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function deleteCase (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/CaseReal/' + data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function list (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/CaseReal', headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors)
      }
    })
}

export function getActives (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/cases-real/actives', headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
