import axios from 'axios'

import { domain } from './data'

const url = domain

export function login (data, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/auth/login', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function logout (data, callBack, errorCallBack) {
  const newToken = data.token.replace('"', ' ')
  axios({ url: url + '/api/v1/auth/logout', data: data, method: 'POST', headers: { Authorization: `Bearer ${newToken}` } })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function getClientData (callBack, errorCallBack) {
  axios({ url: 'https://ip.nf/me.json', method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
