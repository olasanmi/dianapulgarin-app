import axios from 'axios'

import { domain } from './data'

const url = domain

export function suscribeNewsLetter (data, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/newsletter', data: data, headers: { 'Accept': 'application/json; charset=utf-8' }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function getAllNewsLetter (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/newsletter', headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}

export function deleteNewsLetter (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/newsletter/' + data, headers: { 'Accept': 'application/json; charset=utf-8', 'Authorization': `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.errors[0])
      }
    })
}
