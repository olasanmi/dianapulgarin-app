import { applyPolyfills, defineCustomElements } from 'img-comparison-slider/loader'

export default async () => {
  applyPolyfills().then(() => {
    defineCustomElements(window)
  })
}
