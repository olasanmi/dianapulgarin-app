export default {
  home: 'home',
  services: 'services',
  payment: 'payments',
  date: 'appointments',
  blog: 'blog',
  contact: 'contact'
}
